import React from 'react';

const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Users = React.lazy(() => import('./views/Users/Users'));
const User = React.lazy(() => import('./views/Users/User'));
const MasterUsers = React.lazy(() => import('./containers/Master/Users/UsersContainer'));
const MasterTahunAkademik = React.lazy(() => import('./containers/Master/TahunAkademik/TahunAkademikContainer'));
const MasterRuangKelas = React.lazy(() => import('./containers/Master/RuangKelas/RuangKelasContainer'));
const MasterJurusan = React.lazy(() => import('./containers/Master/Jurusan/JurusanContainer'));
const MasterKurikulum = React.lazy(() => import('./containers/Master/Kurikulum/KurikulumContainter'));
const MasterRombel = React.lazy(() => import('./containers/Master/Rombel/RombelContainer'));
const MasterMapel = React.lazy(() => import('./containers/Master/MataPelajaran/MataPelajaranContainer'));



// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  // { path: '/dashboard', name: 'Dashboard', component: Auth(Dashboard, true) },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/master/users', name: 'Users', component: MasterUsers },
  { path: '/master/tahun-akademik', name: 'Tahun Akademik', component: MasterTahunAkademik },
  { path: '/master/ruang-kelas', name: 'Ruang Kelas', component: MasterRuangKelas },
  { path: '/master/jurusan', name: 'Jurusan', component: MasterJurusan },
  { path: '/master/kurikulum', name: 'Kurikulum', component: MasterKurikulum },
  { path: '/master/rombel', name: 'Rombel', component: MasterRombel },
  { path: '/master/mata-pelajaran', name: 'Mata Pelajaran', component: MasterMapel },
  { path: '/users', exact: true,  name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
];

export default routes;
