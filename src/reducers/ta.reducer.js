import { taConstants } from '../constants';

export default function(state={},action){

    switch(action.type){
        case (taConstants.GET_TA):
            return {...state,taList:action.payload}
        case 'USER_REGISTER':
            return {
                ...state,
                register: action.payload.success,
                users: action.payload.users
            }
        default:
            return state;
    }

}