import { combineReducers } from 'redux'
import user from './user_reducer';
import TA from './ta.reducer';

const rootReducer = combineReducers({
    user,
    TA
})

export default rootReducer;