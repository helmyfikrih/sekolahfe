import { userConstants } from '../constants';

export default function(state={},action){

    switch(action.type){
        case (userConstants.GET_USER_ALL):
            return {...state,userList:action.payload}
        case userConstants.LOGIN_REQUEST:
            return { ...state, login: action.payload }
        case 'USER_AUTH':
            return { ...state, login: action.payload }
        case 'USER_MENU':
            return { ...state, menu: action.payload }
        case 'GET_USER_POSTS':
            return { ...state, userPosts: action.payload }
        case 'GET_USER':
            return { ...state, users: action.payload }
        case 'USER_REGISTER':
            return {
                ...state,
                register: action.payload.success,
                users: action.payload.users
            }
        default:
            return state;
    }

}