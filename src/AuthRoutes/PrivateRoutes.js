import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoutes = ({
    render: Rend,
    ...rest
}) => {
    return <Route {...rest} render={(props)=>(
        localStorage.getItem('user') ? 
            <Rend {...props} />
        :
        <Redirect to="/login"/>
    )}/>
}

export default PrivateRoutes;