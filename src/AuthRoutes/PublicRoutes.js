import React from 'react';
import { Route, Redirect } from 'react-router-dom';


const PublicRoutes = ({
    render: Rend,
    ...rest
}) => {
    return <Route {...rest} render={(props)=>(
        rest.restricted ? 
            (localStorage.getItem('user') ? 
                <Redirect to="/dashboard"/>
                : 
                <Rend {...props} />
            )
        :
         <Rend {...props} />
    )}/> 
}

export default PublicRoutes;