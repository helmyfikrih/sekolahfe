import React, { Component } from 'react';
import { userActions } from '../actions'
import {connect} from 'react-redux';

import { css } from '@emotion/core';
// First way to import
import { BounceLoader } from 'react-spinners';

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;`;

export default function(ComposedClass,reload){
    class AuthenticationCheck extends Component {
        state = {
            loading: true
        }

        loading = () => <BounceLoader
            css={override}
            sizeUnit={"px"}
            size={60}
            color={'#3AD1F1'}
            loading={this.state.loading}
        />

        componentWillMount(){
            this.props.dispatch(userActions.auth())
        }

        componentWillReceiveProps(nextProps){
            this.setState({ loading: false })
            if(nextProps.user.login.status !== 200){
                if(reload){
                    this.props.history.push('/login')
                }
            } else {
                if(reload === false) {
                    this.props.history.push('/')
                }
            }
        }

        render(){
            if (this.state.loading) {
                // return <Suspense fallback={this.loading()}></Suspense>
                return <BounceLoader
                    css={override}
                    sizeUnit={"px"}
                    size={60}
                    color={'#3AD1F1'}
                    loading={this.state.loading}
                />
            }
            return (
                    <ComposedClass {...this.props} />
            )
        }
    }

    function mapStateToProps(state){
        return{
            user:state.user
        }
    }
    return connect(mapStateToProps)(AuthenticationCheck)

}