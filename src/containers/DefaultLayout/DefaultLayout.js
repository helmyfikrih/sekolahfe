import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import * as router from 'react-router-dom';
import { Container } from 'reactstrap';
import axios from 'axios';

import {
  AppAside,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
import { NavSekolah } from '../../_nav';
// routes config
import routes from '../../routes';
// import { firebase } from '../../firebase';

import { css } from '@emotion/core';
// First way to import
import { BounceLoader } from 'react-spinners';
import Popper from 'popper.js'

// import { connect } from 'react-redux';
// import { getMenu } from '../../actions';

// see: https://github.com/twbs/bootstrap/issues/23590
Popper.Defaults.modifiers.computeStyle.gpuAcceleration = !(
  window.devicePixelRatio < 1.5 && /Win/.test(navigator.platform)
);

const DefaultAside = React.lazy(() => import('./DefaultAside'));
const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;`;

let axiosInstance = axios.create();
axiosInstance.interceptors.request.use(function (config) {
  const jwt = localStorage.getItem('user');
  const token = "Bearer " + jwt;
  config.headers.Authorization = token;

  return config;
});
const URL_API = process.env.NODE_ENV === 'development' ? '' : process.env.REACT_APP_URL_API;

class DefaultLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  }

  // componentWillMount(){
  //   // axiosInstance.get(`${URL_API}/auth/menu`)
  //   //   .then(response => { 
  //   //     this.setState({
  //   //       items: response.data.data
  //   //     })
  //   //    })
  //   this.props.dispatch(getMenu());
  // }

  loading = () => <BounceLoader
    css={override}
    sizeUnit={"px"}
    size={60}
    color={'#3AD1F1'}
    loading={this.state.loading}
  />

  signOut(e) {
    e.preventDefault()
    axiosInstance.get(`${URL_API}/auth/menu/`, {})
    .then(response => {
      if (response.data.status === 200){
        // console.log(response.data.message)
        localStorage.removeItem('user');
        this.props.history.push('/login')
      }
      })
    
  }
  

  render() {
    let user = 'user';
    // console.log(this.state)
    return (
      <div className="app">
        <AppHeader fixed>
          <Suspense  fallback={this.loading()}>
            <DefaultHeader onLogout={e=>this.signOut(e)}/>
          </Suspense>
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
              <AppSidebarNav navConfig={user === 'admin' ? NavSekolah : NavSekolah} {...this.props} router={router}/>
            </Suspense>
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <AppBreadcrumb appRoutes={routes} router={router}/>
            <Container fluid user={this.props.user}>
              <Suspense fallback={this.loading()}>
                <Switch>
                  {routes.map((route, idx) => {
                    return route.component ? (
                     
                      <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={props => (
                          <route.component {...props} />
                        )} />
                    ) : (null);
                  })}
                  <Redirect from="/" to="/dashboard" />
                </Switch>
              </Suspense>
            </Container>
          </main>
          <AppAside fixed>
            <Suspense fallback={this.loading()}>
              <DefaultAside />
            </Suspense>
          </AppAside>
        </div>
        <AppFooter>
          <Suspense fallback={this.loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter>
      </div>
    );
  }
}

export default DefaultLayout;
// export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
// function mapStateToProps(state) {
//   return {
//     user: state.user
//   }
// }

// export default connect(mapStateToProps)(DefaultLayout)
