import React, { Component } from 'react';
import {
    Badge,
    Button,
    Card,
    CardBody,
    CardHeader,
    Table,
    Form,
    FormGroup,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Collapse
} from 'reactstrap';
import { withFormik } from 'formik';
import * as Yup from 'yup';
// import Select from 'react-select';
import MUIDataTable from "mui-datatables";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import { connect } from 'react-redux';
import { userActions } from '../../../actions';
import { bindActionCreators } from 'redux'
 
const formikEnhancer = withFormik({
    validationSchema: Yup.object().shape({
        email: Yup.string()
            .email("Invalid email address")
            .required("Email is required!"),
        username: Yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required("Username is required!"),
        // topics: Yup.array()
        //     .min(3, "Pick at least 3 tags")
        //     .of(
        //         Yup.object().shape({
        //             label: Yup.string().required(),
        //             value: Yup.string().required()
        //         })
        //     )
    }),
    mapPropsToValues: props => ({
        username: "",
        email: "",
        // topics: []
    }),
    handleSubmit: (values, { setSubmitting }) => {
        console.log(values)
        const payload = {
            ...values,
            // topics: values.topics.map(t => t.value)
        };
        setTimeout(() => {
            alert(JSON.stringify(payload, null, 2));
            setSubmitting(false);
        }, 1000);
    },
    displayName: "MyForm"
});

const MyForm = props => {

    const {
        values,
        touched,
        dirty,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        handleReset,
        // setFieldValue,
        // setFieldTouched,
        isSubmitting
    } = props;

    function getStyles(errors, touched, field) {
        if (errors && touched) {
            return {
                border: '2px solid red'
            }
        } else if (!errors && touched) {
            return {
                border: '2px solid lime'
            }
        }
    }

    return (
        <Form onSubmit={handleSubmit}>
            <FormGroup>
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" id="username" name="username" placeholder="Username" autoComplete="name"
                        value={values.username}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        style={getStyles(errors.username, touched.username, 'username')}
                    />
                </InputGroup>
                {errors.username && touched.username && (
                    <div style={{ color: "red", marginTop: ".5rem" }}>{errors.username}</div>
                )}
            </FormGroup>
            <FormGroup>
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <InputGroupText><i className="fa fa-envelope"></i></InputGroupText>
                    </InputGroupAddon>
                    <Input type="email" id="email" name="email" placeholder="Email" autoComplete="username"
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        style={getStyles(errors.email, touched.email, 'email')}
                    />
                </InputGroup>
                {errors.email && touched.email && (
                    <div style={{ color: "red", marginTop: ".5rem" }}>{errors.email}</div>
                )}
            </FormGroup>
            <FormGroup>
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <InputGroupText><i className="fa fa-asterisk"></i></InputGroupText>
                    </InputGroupAddon>
                    <Input type="password" id="password1" name="password1" placeholder="Password" autoComplete="current-password" />
                </InputGroup>
            </FormGroup>
            {/* <MySelect
                                value={values.topics}
                                onChange={setFieldValue}
                                onBlur={setFieldTouched}
                                error={errors.topics}
                                touched={touched.topics}
                            /> */}
            <FormGroup className="form-actions">
                <Button type="submit" size="sm" color="success"
                    onClick={handleReset}
                    disabled={!dirty || isSubmitting}>Reset</Button>
                <Button type="submit" size="sm" color="success" disabled={isSubmitting}>Submit</Button>
            </FormGroup>
        </Form>
    );
};
const MyEnhancedForm = formikEnhancer(MyForm);

const INITIAL_STATE = {
    loading: false,
    collapse: false
};

class UsersContainer extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = { ...INITIAL_STATE };

    }

    componentWillMount(){
        console.log(this.props)
        this.props.dispatch(userActions.userListAll())
    }

    toggle() {
        this.setState(state => ({ collapse: !state.collapse }));
    }


    render() {
        const getBadge = (status) => {
            return status === 'Active' ? 'success' :
                status === 'Inactive' ? 'secondary' :
                    status === 'Pending' ? 'warning' :
                        status === 'Banned' ? 'danger' :
                            'primary'
        }
        const URL_API = process.env.NODE_ENV === 'development' ? '' : process.env.REACT_APP_URL_API;
        const columns = [
            'id',
            {
                name:'name',
                label:'Name',
                options: {
                    filter: false,
                    sort: false
                }
            }, 
                "registered", "role", 
            {
                name:'status',
                label:'Status',
                options: {
                    filter: true,
                    customBodyRender: (value, tableMeta, updateValue) => (
                        <Badge color={getBadge(value)}>{value}</Badge>
                    )
                }
            }];

        // const data = [
        //     ["Gabby George", "Business Analyst", "Minneapolis", 30, "$100,000"],
        //     ["Aiden Lloyd", "Business Consultant", "Dallas", 55, "$200,000"],
        //     ["Jaden Collins", "Attorney", "Santa Ana", 27, "$500,000"],
        //     ["Franky Rees", "Business Analyst", "St. Petersburg", 22, "$50,000"],
        //     ["Aaren Rose", "Business Consultant", "Toledo", 28, "$75,000"],
        //     [
        //         "Blake Duncan",
        //         "Business Management Analyst",
        //         "San Diego",
        //         65,
        //         "$94,000"
        //     ],
        //     ["Frankie Parry", "Agency Legal Counsel", "Jacksonville", 71, "$210,000"],
        //     ["Lane Wilson", "Commercial Specialist", "Omaha", 19, "$65,000"],
        //     ["Robin Duncan", "Business Analyst", "Los Angeles", 20, "$77,000"],
        //     ["Mel Brooks", "Business Consultant", "Oklahoma City", 37, "$135,000"],
        //     ["Harper White", "Attorney", "Pittsburgh", 52, "$420,000"],
        //     ["Kris Humphrey", "Agency Legal Counsel", "Laredo", 30, "$150,000"],
        //     ["Frankie Long", "Industrial Analyst", "Austin", 31, "$170,000"],
        //     ["Brynn Robbins", "Business Analyst", "Norfolk", 22, "$90,000"],
        //     ["Justice Mann", "Business Consultant", "Chicago", 24, "$133,000"],
        //     [
        //         "Addison Navarro",
        //         "Business Management Analyst",
        //         "New York",
        //         50,
        //         "$295,000"
        //     ],
        //     ["Jesse Welch", "Agency Legal Counsel", "Seattle", 28, "$200,000"],
        //     ["Eli Mejia", "Commercial Specialist", "Long Beach", 65, "$400,000"],
        //     ["Gene Leblanc", "Industrial Analyst", "Hartford", 34, "$110,000"],
        //     ["Danny Leon", "Computer Scientist", "Newark", 60, "$220,000"],
        //     ["Lane Lee", "Corporate Counselor", "Cincinnati", 52, "$180,000"],
        //     ["Jesse Hall", "Business Analyst", "Baltimore", 44, "$99,000"],
        //     ["Danni Hudson", "Agency Legal Counsel", "Tampa", 37, "$90,000"],
        //     ["Terry Macdonald", "Commercial Specialist", "Miami", 39, "$140,000"],
        //     ["Justice Mccarthy", "Attorney", "Tucson", 26, "$330,000"],
        //     ["Silver Carey", "Computer Scientist", "Memphis", 47, "$250,000"],
        //     ["Franky Miles", "Industrial Analyst", "Buffalo", 49, "$190,000"],
        //     ["Glen Nixon", "Corporate Counselor", "Arlington", 44, "$80,000"],
        //     [
        //         "Gabby Strickland",
        //         "Business Process Consultant",
        //         "Scottsdale",
        //         26,
        //         "$45,000"
        //     ],
        //     ["Mason Ray", "Computer Scientist", "San Francisco", 39, "$142,000"]
        // ];
        
        const options = {
            filterType: "dropdown",
            responsive: "scroll",
            // onTableChange: (action, tableState) => {
            //     // this.xhrRequest('my.api.com/tableData', result => {
            //     //     this.setState({ data: result });
            //     // });
            //     console.log(tableState)
            //     console.log(action)
            // }
            expandableRows: true,
            expandableRowsOnClick: true,
            // rowsExpanded: [0, 2, 3],
            renderExpandableRow: (rowData, rowMeta) => {
                const colSpan = rowData.length + 1;
                const user = this.props.users.userList.find(user => user.id === rowData[0])
                const userDetails = user ? Object.entries(user) : [['id', (<span><i className="text-muted icon-ban"></i> Not found</span>)]]
                return (
                    <TableRow>
                        <TableCell colSpan={colSpan}>
                            <Card>
                                <CardHeader>
                                    <strong><i className="icon-info pr-1"></i>User id: {rowData.id}</strong>
                                </CardHeader>
                                <CardBody>
                                    <Table responsive striped hover>
                                        <tbody>
                                            {
                                                userDetails.map(([key, value]) => {
                                                    return (
                                                        <tr key={key}>
                                                            <td>{`${key}:`}</td>
                                                            <td><strong>{value.toString()}</strong></td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </Table>
                                </CardBody>
                            </Card>
                        </TableCell>
                    </TableRow>
                );
            }
        };

        const theme = createMuiTheme({
            palette: { type: 'light' },
            typography: { useNextVariants: true },
        });

        return (
            <div className="animated fadeIn">
                <Collapse isOpen={this.state.collapse}>
                    <Card>
                        <CardHeader>
                            <strong>Add New USer</strong>
                            <Button color="ghost-primary" size="sm" className="float-right" onClick={this.toggle}>
                                <i className="fa fa-close"></i>&nbsp;Close
                            </Button>
                        </CardHeader>
                        <CardBody>
                            <MyEnhancedForm />
                        </CardBody>
                    </Card>
                </Collapse>

                <Card>
                    <CardHeader>
                        <strong>Data Master Users</strong>
                        <Button color="ghost-primary" size="sm" className="float-right" onClick={this.toggle}>
                            <i className="fa fa-plus"></i>&nbsp;Add New User
                        </Button>
                    </CardHeader>
                    <CardBody>
                        <MuiThemeProvider theme={theme}>
                            <MUIDataTable
                                title={"Users List"}
                                data={this.props.users.userList}
                                columns={columns}
                                options={options}
                            />
                        </MuiThemeProvider>
                       
                        {/* <MaterialTable
                            columns={[
                                // { title: "id", field: "id" },
                                { title: "name", field: "name" },
                                { title: "registered", field: "registered" },
                                { title: "role", field: "role" },
                                { title: "status", field: "status", render: rowData => <Badge color={getBadge(rowData.status)}>{rowData.status}</Badge> },
                                // {
                                // title: "Doğum Yeri",
                                // field: "birthCity",
                                // lookup: { 34: "İstanbul", 63: "Şanlıurfa" }
                                // }
                            ]}
                            data={
                                // this.props.users.userList
                                query =>
                                new Promise((resolve, reject) => {
                                    let url = `${URL_API}/user/show/`
                                    // url += 'per_page=' + query.pageSize
                                    // url += '&page=' + (query.page + 1)
                                    fetch(url, {
                                        method: "post",
                                        headers: {
                                            'Accept': 'application/json',
                                            'Content-Type': 'application/json',
                                            'Authorization': 'Bearer ' + localStorage.getItem('user')
                                        },

                                        //make sure to serialize your JSON body
                                        body: JSON.stringify({
                                        })
                                        }) 
                                        .then(response => response.json())
                                        .then(result => {
                                            resolve({
                                                data: result.data,
                                                page: result.page - 1,
                                                totalCount: result.total,
                                            })
                                        })
                                })
                            }
                            actions={[
                                {
                                    icon: 'save',
                                    tooltip: 'Save User',
                                    onClick: (event, rowData, type) => {
                                        if(type === 'delete'){
                                            alert("You "+ type+" " + rowData.id)
                                        }else if (type==='save'){
                                            alert("You saved " + type + " " +  rowData.id)
                                        }
                                    }
                                },
                                // {
                                //     icon: 'refresh',
                                //     tooltip: 'Refresh Data',
                                //     isFreeAction: true,
                                //     onClick: () => this.tableRef.current && this.tableRef.current.onQueryChange(),
                                // }
                            ]}
                            components={{
                                Action: props => (
                                    <div>
                                        <Button color="ghost-primary" size="sm"
                                            onClick={(event) => props.action.onClick(event, props.data, 'delete')}
                                        >
                                            <i className="fa fa-trash"></i>&nbsp;Delete
                                        </Button>
                                        <Button color="ghost-primary" size="sm"
                                            onClick={(event) => props.action.onClick(event, props.data, 'save')}
                                        >
                                            <i className="fa fa-trash"></i>&nbsp;Save
                                        </Button>
                                    </div>
                                   
                                ),
                            }}
                            title="Users Data"
                            detailPanel={[
                                {
                                    icon: 'account_circle',
                                    tooltip: 'Show Detail',
                                    render: rowData => {

                                        const user = this.props.users.userList.find(user => user.id === rowData.id)
                                        const userDetails = user ? Object.entries(user) : [['id', (<span><i className="text-muted icon-ban"></i> Not found</span>)]]
                                        return (
                                            <div style={{ maxWidth: "100%" }}>
                                                <Card>
                                                    <CardHeader>
                                                        <strong><i className="icon-info pr-1"></i>User id: {rowData.id}</strong>
                                                    </CardHeader>
                                                    <CardBody>
                                                        <Table responsive striped hover>
                                                            <tbody>
                                                                {
                                                                    userDetails.map(([key, value]) => {
                                                                        return (
                                                                            <tr key={key}>
                                                                                <td>{`${key}:`}</td>
                                                                                <td><strong>{value.toString()}</strong></td>
                                                                            </tr>
                                                                        )
                                                                    })
                                                                }
                                                            </tbody>
                                                        </Table>
                                                    </CardBody>
                                                </Card>
                                            </div>

                                        )
                                    },
                                },
                            ]}
                            // onRowClick={(event, rowData, togglePanel) => togglePanel()}
                        /> */}
                    </CardBody>
                </Card>
            </div>
        )
    }
}

// function mapStateToProps(state){
//     return{
//         users:state.user
//     }
// }

// function mapDispatchToProps(dispatch){
//     return bindActionCreators(userActions.userListAll(),dispatch)
// }

// export default connect(mapStateToProps, mapDispatchToProps)(UsersContainer);

function mapStateToProps(state) {
    return {
        users: state.user
    }
}

export default connect(mapStateToProps)(UsersContainer)
