import React, { Component } from 'react';
import {
    Badge,
    Button,
    Card,
    CardBody,
    CardHeader,
    Table,
    Form,
    FormGroup,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Collapse
} from 'reactstrap';
import { withFormik } from 'formik';
import * as Yup from 'yup';
// import Select from 'react-select';
import MUIDataTable from "mui-datatables";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

import { connect } from 'react-redux';
import { taActions } from '../../../actions';

const formikEnhancer = withFormik({
    validationSchema: Yup.object().shape({
        email: Yup.string()
            .email("Invalid email address")
            .required("Email is required!"),
        username: Yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required("Username is required!"),
        // topics: Yup.array()
        //     .min(3, "Pick at least 3 tags")
        //     .of(
        //         Yup.object().shape({
        //             label: Yup.string().required(),
        //             value: Yup.string().required()
        //         })
        //     )
    }),
    mapPropsToValues: props => ({
        username: "",
        email: "",
        // topics: []
    }),
    handleSubmit: (values, { setSubmitting }) => {
        console.log(values)
        const payload = {
            ...values,
            // topics: values.topics.map(t => t.value)
        };
        setTimeout(() => {
            alert(JSON.stringify(payload, null, 2));
            setSubmitting(false);
        }, 1000);
    },
    displayName: "MyForm"
});

const MyForm = props => {

    const {
        values,
        touched,
        dirty,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        handleReset,
        // setFieldValue,
        // setFieldTouched,
        isSubmitting
    } = props;

    function getStyles(errors, touched, field) {
        if (errors && touched) {
            return {
                border: '2px solid red'
            }
        } else if (!errors && touched) {
            return {
                border: '2px solid lime'
            }
        }
    }

    return (
        <Form onSubmit={handleSubmit}>
            <FormGroup>
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" id="username" name="username" placeholder="Username" autoComplete="name"
                        value={values.username}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        style={getStyles(errors.username, touched.username, 'username')}
                    />
                </InputGroup>
                {errors.username && touched.username && (
                    <div style={{ color: "red", marginTop: ".5rem" }}>{errors.username}</div>
                )}
            </FormGroup>
            <FormGroup>
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <InputGroupText><i className="fa fa-envelope"></i></InputGroupText>
                    </InputGroupAddon>
                    <Input type="email" id="email" name="email" placeholder="Email" autoComplete="username"
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        style={getStyles(errors.email, touched.email, 'email')}
                    />
                </InputGroup>
                {errors.email && touched.email && (
                    <div style={{ color: "red", marginTop: ".5rem" }}>{errors.email}</div>
                )}
            </FormGroup>
            <FormGroup>
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <InputGroupText><i className="fa fa-asterisk"></i></InputGroupText>
                    </InputGroupAddon>
                    <Input type="password" id="password1" name="password1" placeholder="Password" autoComplete="current-password" />
                </InputGroup>
            </FormGroup>
            {/* <MySelect
                                value={values.topics}
                                onChange={setFieldValue}
                                onBlur={setFieldTouched}
                                error={errors.topics}
                                touched={touched.topics}
                            /> */}
            <FormGroup className="form-actions">
                <Button type="submit" size="sm" color="success"
                    onClick={handleReset}
                    disabled={!dirty || isSubmitting}>Reset</Button>
                <Button type="submit" size="sm" color="success" disabled={isSubmitting}>Submit</Button>
            </FormGroup>
        </Form>
    );
};
const MyEnhancedForm = formikEnhancer(MyForm);

const INITIAL_STATE = {
    loading: false,
    collapse: false
};

class KurikulumContainer extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = { ...INITIAL_STATE };

    }

    componentWillMount() {
        this.props.dispatch(taActions.TahunAkademikAll())
    }

    toggle() {
        this.setState(state => ({ collapse: !state.collapse }));
    }


    render() {
        const getBadge = (status) => {
            return status === 'Active' ? 'success' :
                status === 'Inactive' ? 'secondary' :
                    status === 'Pending' ? 'warning' :
                        status === 'Banned' ? 'danger' :
                            'primary'
        }
        const URL_API = process.env.NODE_ENV === 'development' ? '' : process.env.REACT_APP_URL_API;
        const columns = [
            'id',
            {
                name: 'name',
                label: 'Name',
                options: {
                    filter: false,
                    sort: false
                }
            },
            "registered", "role",
            {
                name: 'status',
                label: 'Status',
                options: {
                    filter: true,
                    customBodyRender: (value, tableMeta, updateValue) => (
                        <Badge color={getBadge(value)}>{value}</Badge>
                    )
                }
            }];

        const options = {
            filterType: "dropdown",
            responsive: "scroll",
            // onTableChange: (action, tableState) => {
            //     // this.xhrRequest('my.api.com/tableData', result => {
            //     //     this.setState({ data: result });
            //     // });
            //     console.log(tableState)
            //     console.log(action)
            // }
            expandableRows: true,
            expandableRowsOnClick: true,
            // rowsExpanded: [0, 2, 3],
            renderExpandableRow: (rowData, rowMeta) => {
                const colSpan = rowData.length + 1;
                const user = this.props.TA.taList.find(user => user.id === rowData[0])
                const userDetails = user ? Object.entries(user) : [['id', (<span><i className="text-muted icon-ban"></i> Not found</span>)]]

                return (
                    <TableRow>
                        <TableCell colSpan={colSpan}>
                            <Card>
                                <CardHeader>
                                    <strong><i className="icon-info pr-1"></i>User id: {rowData.id}</strong>
                                </CardHeader>
                                <CardBody>
                                    <Table responsive striped hover>
                                        <tbody>
                                            {
                                                userDetails.map(([key, value]) => {
                                                    return (
                                                        <tr key={key}>
                                                            <td>{`${key}:`}</td>
                                                            <td><strong>{value.toString()}</strong></td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </Table>
                                </CardBody>
                            </Card>
                        </TableCell>
                    </TableRow>
                );
            }
        };

        const theme = createMuiTheme({
            palette: { type: 'light' },
            typography: { useNextVariants: true },
        });
        return (
            <div className="animated fadeIn">
                <Collapse isOpen={this.state.collapse}>
                    <Card>
                        <CardHeader>
                            <strong>Add New Tahun Akademik</strong>
                            <Button color="ghost-primary" size="sm" className="float-right" onClick={this.toggle}>
                                <i className="fa fa-close"></i>&nbsp;Close
                            </Button>
                        </CardHeader>
                        <CardBody>
                            <MyEnhancedForm />
                        </CardBody>
                    </Card>
                </Collapse>

                <Card>
                    <CardHeader>
                        <strong>Data Master Tahun Akademik</strong>
                        <Button color="ghost-primary" size="sm" className="float-right" onClick={this.toggle}>
                            <i className="fa fa-plus"></i>&nbsp;Add New Tahun Akademik
                        </Button>
                    </CardHeader>
                    <CardBody>
                        <MuiThemeProvider theme={theme}>
                            <MUIDataTable
                                title={"Tahun Akademik List"}
                                data={this.props.TA.taList}
                                columns={columns}
                                options={options}
                            />
                        </MuiThemeProvider>
                    </CardBody>
                </Card>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        TA: state.TA
    }
}

export default connect(mapStateToProps)(KurikulumContainer)
