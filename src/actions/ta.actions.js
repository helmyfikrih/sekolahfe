import usersData from '../views/Master/Users/UsersData';
import { taConstants } from '../constants';

function TahunAkademikAll() {

    const request = usersData;

    return {
        type: taConstants.GET_TA,
        payload: request
    }
}


export const taActions = {
    TahunAkademikAll,
    // delete: _delete
};
 
