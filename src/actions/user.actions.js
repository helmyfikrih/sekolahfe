import usersData from '../views/Master/Users/UsersData';
import axios from 'axios';
import { userConstants } from '../constants';
// import { URL_API } from '../config';
// import * as jwt_decode from 'jwt-decode';
// Add a request interceptor

let axiosInstance = axios.create();
axiosInstance.interceptors.request.use(function (config) {
    const jwt = localStorage.getItem('user');
    const token = "Bearer " + jwt;
    config.headers.Authorization = token;

    return config;
});

const URL_API = process.env.NODE_ENV === 'development' ? '' : process.env.REACT_APP_URL_API ;

/*========= USER ===========*/

function loginUser({username,password}){
    const request = axios.post(`${URL_API}/auth/login`, {
        userId: username,
        password: password
    })
        .then(response => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            if(response.data.status===200){
                localStorage.setItem('user', response.data.data.token);
            }

            return response.data;
        });
        
    return {
        type: userConstants.LOGIN_REQUEST,
        payload:request
    }
}

function auth(){
    let request = null;
    let isExpired = false;
    const jwt = localStorage.getItem('user');
    if(jwt){
        if(isExpired) {
            request = { status: 401 };
        }else{
            request = axiosInstance.post(`${URL_API}/auth/secure/validate/`, {})
                .then(response => {
                    return response.data;
                })
        }
    } else {
        request = { status: 401 };
    }

    return {
        type:'USER_AUTH',
        payload:request
    }

}

function getMenu(){
    const request = axiosInstance.get(`${URL_API}/auth/menu/`, {})
        .then(response => response.data.data)

    return {
        type:'USER_MENU',
        payload:request
    }

}

function getUsers(){
    const request = axios.get(`/api/users`)
                    .then(response => response.data);
    return {
        type:'GET_USER',
        payload:request
    }
}


function userRegister(user,userList){
    const request = axios.post(`/api/register`,user)

    return (dispatch) =>{
        request.then(({data})=>{
            let users = data.success ? [...userList,data.user]:userList;
            let response = {
                success:data.success,
                users
            }

            dispatch({
                type:'USER_REGISTER',
                payload:response
            })
        })
    }
}

function userListAll(){

    const request = usersData;

    return {
        type: userConstants.GET_USER_ALL,
        payload: request
    }
}


export const userActions = {
    loginUser,
    auth,
    getMenu,
    getUsers,
    userRegister,
    userListAll
    // delete: _delete
};
