const NavAdmin = {
  items: [
    {
      name: 'Home',
      url: '/dashboard',
      icon: 'icon-home',
      badge: {
        variant: 'info',
        // text: 'NEW',
      },
    },
    {
      name: 'Jenis Pembayaran Sekolah',
      url: '/data_karywan',
      icon: 'icon-layers'
    },
    {
      name: 'Setup Biaya Pembayaran',
      url: '/data_karywan',
      icon: 'icon-layers'
    },
    {
      name: 'PSB',
      url: '/data_karywan',
      icon: 'icon-layers'
    },
    {
      name: 'Data Karyawan',
      url: '/data_karywan',
      icon: 'icon-layers'
    },
    {
      name: 'Data Siswa',
      url: '/data_karywan',
      icon: 'icon-layers'
    },
    {
      name: 'Data Jadwal Pelajaran',
      url: '/data_karywan',
      icon: 'icon-layers'
    },
    {
      name: 'Data Jadwal Guru',
      url: '/data_karywan',
      icon: 'icon-layers'
    },
    {
      name: 'Data Wali Kelas',
      url: '/data_karywan',
      icon: 'icon-layers'
    },
    {
      name: 'Data Ujian Online',
      url: '/data_karywan',
      icon: 'icon-layers'
    },
    {
      name: 'Data Absensi',
      url: '/data_karywan',
      icon: 'icon-layers'
    },
    {
      name: 'Kenaikan Kelas',
      url: '/data_karywan',
      icon: 'icon-layers'
    },
    {
      name: 'Master Data',
      url: '/master',
      icon: 'icon-layers',
      children: [{
        name: 'Master User',
        url: '/master/users',
        icon: 'icon-user',
      },
      {
        name: 'Tahun Akademik',
        url: '/master/tahun-akademik',
        icon: 'icon-user',
      },
      {
        name: 'Ruang Kelas',
        url: '/master/ruang-kelas',
        icon: 'icon-user',
      },
      {
        name: 'Jurusan',
        url: '/master/jurusan',
        icon: 'icon-user',
      },
      {
        name: 'Kurikulum',
        url: '/master/kurikulum',
        icon: 'icon-user',
      },
      {
        name: 'Rombel',
        url: '/master/rombel',
        icon: 'icon-user',
      },
      {
        name: 'Mata Pelajaran',
        url: '/master/mata-pelajaran',
        icon: 'icon-user',
      }]
    },
    // {
    //   title: true,
    //   name: 'Theme',
    //   wrapper: {            // optional wrapper object
    //     element: '',        // required valid HTML5 element tag
    //     attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
    //   },
    //   class: ''             // optional class names space delimited list for title item ex: "text-center"
    // },
    {
      name: 'Users',
      url: '/users',
      icon: 'icon-user',
    },

  ],
}

const getItem = {
    items: [
      {
        name: 'Home',
        url: '/dashboard',
        icon: 'icon-home',
        badge: {
          variant: 'info',
          // text: 'NEW',
        },
      },
      {
        name: 'Jenis Pembayaran Sekolah',
        url: '/data_karywan',
        icon: 'icon-layers'
      },
    ]
  }

const NavUser = getItem;

const NavSekolah = {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-home',
      badge: {
        variant: 'info'
      }
    },
    {
      name: 'Karyawan',
      url: '/karyawan',
      icon: 'icon-layers',
      children: [
        {
          name: 'Data Karyawan',
          url: '/karyawan/data-karyawan',
          icon: 'icon-user'
        },
        {
          name: 'Jabatan',
          url: '/karyawan/data-jabatan',
          icon: 'icon-user'
        }
      ]
    },
    {
      name: 'Siswa',
      url: '/siswa',
      icon: 'icon-layers',
      children: [
        {
          name: 'Data Siswa',
          url: '/siswa/data-siswa',
          icon: 'icon-user'
        },
        {
          name: 'Alumni',
          url: '/siswa/data-alumni',
          icon: 'icon-user'
        }
      ]
    },
    {
      name: 'Kelas',
      url: '/kelas',
      icon: 'icon-layers',
      children: [
        {
          name: 'Data Kelas',
          url: '/kelas/data-kelas',
          icon: 'icon-user'
        },
        {
          name: 'Peserta Kelas',
          url: '/kelas/data-peserta-kelas',
          icon: 'icon-user'
        },
        {
          name: 'Kenaikan Kelas',
          url: '/kelas/data-kenaikan-kelas',
          icon: 'icon-user'
        },
        {
          name: 'Jurusan',
          url: '/kelas/data-jurusan',
          icon: 'icon-user'
        }
      ]
    },
    {
      name: 'Kurikulum',
      url: '/kurikulum',
      icon: 'icon-layers',
      children: [
        {
          name: 'Struktur Kurikulum',
          url: '/kurikulum/struktur-kurikulum',
          icon: 'icon-user'
        },
        {
          name: 'Jam Pelajaran',
          url: '/kurikulum/jam-pelajaran',
          icon: 'icon-user'
        },
        {
          name: 'Kegiatan & Ekskul',
          url: '/kurikulum/kegiatan-ekskul',
          icon: 'icon-user'
        },
        {
          name: 'Jadwal Mapel Priority',
          url: '/kurikulum/jadwal-mapel-priority',
          icon: 'icon-user'
        },
        {
          name: 'Tahun Ajaran',
          url: '/kurikulum/tahun-ajaran',
          icon: 'icon-user'
        },
        {
          name: 'Mata Pelajran',
          url: '/kurikulum/mata-pelajaran',
          icon: 'icon-user'
        },
        {
          name: 'Program Studi',
          url: '/kurikulum/program-studi',
          icon: 'icon-user'
        }
      ]
    },
    {
      name: 'Jadwal',
      url: '/jadwal',
      icon: 'icon-layers',
      children: [
        {
          name: 'Jadwal Kelas',
          url: '/jadwal/jadwal-kelas',
          icon: 'icon-user'
        },
        {
          name: 'Jadwal Mengajar Guru',
          url: '/jadwal/jadwal-mengajar-guru',
          icon: 'icon-user'
        },
        {
          name: 'Jadwal Pembayaran',
          url: '/jadwal/kegiatan-ekskul',
          icon: 'icon-user'
        },
      ]
    },
    {
      name: 'Pembayaran',
      url: '/pembayaran',
      icon: 'icon-layers',
      children: [
        {
          name: 'Tagihan Pembayaran',
          url: '/pembayaran/tagihan-pembayaran',
          icon: 'icon-user'
        },
        {
          name: 'Daftar Pembayaran',
          url: '/pembayaran/daftar-pembayaran',
          icon: 'icon-user'
        },
        {
          name: 'Jenis Pembayaran',
          url: '/pembayaran/jenis-pembayaran',
          icon: 'icon-user'
        },
        {
          name: 'Auto Debit',
          url: '/pembayaran/auto-debit',
          icon: 'icon-user'
        },
        {
          name: 'PLN',
          url: '/pembayaran/pln',
          icon: 'icon-user'
        },
         {
          name: 'Halo',
          url: '/pembayaran/halo',
          icon: 'icon-user'
        },
        {
          name: 'Indosat Ooredoo',
          url: '/pembayaran/Indosat-Ooredoo',
          icon: 'icon-user'
        },
        {
          name: 'Xl Xplor',
          url: '/pembayaran/xl-xplor',
          icon: 'icon-user'
        },
        {
          name: 'PDAM',
          url: '/pembayaran/pdam',
          icon: 'icon-user'
        },
        {
          name: 'Multifinance',
          url: '/pembayaran/multifinance',
          icon: 'icon-user'
        },
      ]
    },
    {
      name: 'Absensi',
      url: '/absensi',
      icon: 'icon-layers',
      children: [
        {
          name: 'Absensi',
          url: '/absensi/data-absensi',
          icon: 'icon-user'
        },
        {
          name: 'Absensi',
          url: '/absensi/daftar-absensi',
          icon: 'icon-user'
        },
      ]
    },
    {
      name: 'Penilaian',
      url: '/penilaian',
      icon: 'icon-layers',
      children: [
        {
          name: 'Ekstra Kulikuler',
          url: '/penilaian/ekstra-kulikuler',
          icon: 'icon-user'
        },
        {
          name: 'Data Raport Wali Kelas',
          url: '/penilaian/raport-wali-kelas',
          icon: 'icon-user'
        },
        {
          name: 'Raport Siswa',
          url: '/penilaian/raport-siswa',
          icon: 'icon-user'
        },
        {
          name: 'Ranking',
          url: '/penilaian/ranking',
          icon: 'icon-user'
        },
        {
          name: 'Jenis Penilaian',
          url: '/penilaian/jenis-penilaian',
          icon: 'icon-user'
        },
        {
          name: 'Predikat Nilai',
          url: '/penilaian/predikat-nilai',
          icon: 'icon-user'
        },
        {
          name: 'Jenis Ujian',
          url: '/penilaian/jenis-ujian',
          icon: 'icon-user'
        },
        {
          name: 'Data Penilaian Per Siswa',
          url: '/penilaian/data-penilaian-siswa',
          icon: 'icon-user'
        },
        {
          name: 'Data Penilaian Per Wali Kelas',
          url: '/penilaian/data-penilaian-wali-kelas',
          icon: 'icon-user'
        },
        {
          name: 'Data Penilaian Per Guru',
          url: '/penilaian/data-penilaian-guru',
          icon: 'icon-user'
        },
      ]
    },
    {
      name: 'Dompet Azkiya',
      url: '/dompet-azkiya',
      icon: 'icon-layers',
      children: [
        {
          name: 'Topup',
          url: '/dompet-azkiya/topup',
          icon: 'icon-user'
        },
        {
          name: 'Pencarian',
          url: '/dompet-azkiya/pencairan',
          icon: 'icon-user'
        },
        {
          name: 'Transfer',
          url: '/dompet-azkiya/transfer',
          icon: 'icon-user'
        },
      ]
    },
    {
      name: 'Laporan',
      url: '/laporan',
      icon: 'icon-layers',
      children: [
        {
          name: 'Rekap Absensi',
          url: '/laporan/topup',
          icon: 'icon-user'
        },
        {
          name: 'Absensi Per Siwa',
          url: '/laporan/absensi-siswa',
          icon: 'icon-user'
        },
      ]
    },
    {
      name: 'Setting',
      url: '/setting',
      icon: 'icon-layers',
      children: [
        {
          name: 'Profile Sekolah',
          url: '/setting/topup',
          icon: 'icon-user'
        },
      ]
    },
    {
      name: 'Master Data',
      url: '/master',
      icon: 'icon-layers',
      children: [{
        name: 'Master User',
        url: '/master/users',
        icon: 'icon-user',
      },
      {
        name: 'Tahun Akademik',
        url: '/master/tahun-akademik',
        icon: 'icon-user',
      },
      {
        name: 'Ruang Kelas',
        url: '/master/ruang-kelas',
        icon: 'icon-user',
      },
      {
        name: 'Jurusan',
        url: '/master/jurusan',
        icon: 'icon-user',
      },
      {
        name: 'Kurikulum',
        url: '/master/kurikulum',
        icon: 'icon-user',
      },
      {
        name: 'Rombel',
        url: '/master/rombel',
        icon: 'icon-user',
      },
      {
        name: 'Mata Pelajaran',
        url: '/master/mata-pelajaran',
        icon: 'icon-user',
      }]
    }
  ]
}

export {
  NavAdmin,
  NavUser,
  NavSekolah
};
