import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import axios from 'axios';
import { css } from '@emotion/core';
// First way to import
import { BeatLoader } from 'react-spinners';
import { URL_API } from '../../../config';

// import { connect } from 'react-redux';
// import { userLogin } from '../../../actions';
// import { bindActionCreators } from 'redux'

// import { firebase } from '../../../firebase';


// Can be a string as well. Need to ensure each key-value pair ends with ;
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const INITIAL_STATE = {
  username: '',
  password: '',
  error: null,
  loading: false
};

class LoginContainer extends Component {

  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = { ...INITIAL_STATE };
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit(event) {
    const { username, password } = this.state;
    this.setState({
      error: null,
      loading: true
    })
    // firebase.auth()
    //   .signInWithEmailAndPassword(email, password)
    //   .then(() => {
    //     this.props.history.push('/')

    //   })
    //   .catch(error => {
    //     this.setState({ 
    //       error,
    //       loading: false });
    //   });
    if (username && password) {
      axios.post(`${URL_API}/auth/login`, {
        userId: username,
        password: password
      })
        .then(response => {
          if (response.data.status !== 200) {
            this.setState({
              error: response.data,
              loading: false
            })
          } else {
            localStorage.setItem('user', JSON.stringify(response.data.data));
            this.props.history.push('/')
            // console.log(this.props)
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
    event.preventDefault();
  }



  render() {
    const { username, password, error } = this.state;

    // const isInvalid = password === '' || email === '';
    const isLoading = this.state.loading ? <BeatLoader
      css={override}
      sizeUnit={"px"}
      size={15}
      color={'#fff'}
      loading={this.state.loading}
    /> : 'Login';

    const isError = this.state.error ?
      <p style={{ color: 'red' }}>{error.message}</p>
      : null;

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={this.handleSubmit}>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="text"
                          placeholder="username"
                          autoComplete="username"
                          name="username"
                          defaultValue={username}
                          onChange={this.handleChange} />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          placeholder="Password"
                          autoComplete="current-password"
                          name="password"
                          defaultValue={password}
                          onChange={this.handleChange} />
                      </InputGroup>
                      {/* {showError()}
                      {error && <p style={{color:'red'}}>{error.message}</p>} */}
                      {isError}
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4" onClick={this.handleSubmit}>{isLoading}</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                      <Link to="/register">
                        <Button color="primary" className="mt-3" active tabIndex={-1}>Register Now!</Button>
                      </Link>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

// function mapStateToProps(state) {
//     return {
//         users: state.users
//     }
// }

// function mapDispatchToProps(dispatch) {
//     return bindActionCreators({ userLogin }, dispatch)
// }

// export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
export default LoginContainer;
