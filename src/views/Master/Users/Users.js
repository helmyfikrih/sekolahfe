import React from 'react';

import UsersContainer from '../../../containers/Master/Users/UsersContainer';

const Users = () => {
    return(
        <div>
            <UsersContainer/>
        </div>
    )
}

export default Users;