import * as firebase from 'firebase';

var Config = {
    apiKey: "AIzaSyD1fIp7P5JbP5WRos1VJSxsxaQ6-nbxpv4",
    authDomain: "sekolahku-53fc0.firebaseapp.com",
    databaseURL: "https://sekolahku-53fc0.firebaseio.com",
    projectId: "sekolahku-53fc0",
    storageBucket: "",
    messagingSenderId: "879276977733",
    appId: "1:879276977733:web:6e8a89cc4feed01d"
};

firebase.initializeApp(Config);

const firebaseDB = firebase.database();

export {
    firebase,
    firebaseDB
}