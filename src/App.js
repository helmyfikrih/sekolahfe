import React, { Component } from 'react';
import { HashRouter, Switch, Route } from 'react-router-dom';
import Auth from './AuthRoutes/Auth';
// import { renderRoutes } from 'react-router-config';
// import PrivateRoutes from './AuthRoutes/PrivateRoutes';
// import PublicRoutes from './AuthRoutes/PublicRoutes';
import './App.scss';
import { css } from '@emotion/core';
// First way to import
import { BounceLoader } from 'react-spinners';

// Containers
const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'));

// Pages
const Login = React.lazy(() => import('./containers/Page/login'));
const Register = React.lazy(() => import('./views/Pages/Register'));
const Page404 = React.lazy(() => import('./views/Pages/Page404'));
const Page500 = React.lazy(() => import('./views/Pages/Page500'));

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  }
  
  render() {
    // Can be a string as well. Need to ensure each key-value pair ends with ;
    const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;`;

    const loading = () => <BounceLoader
      css={override}
      sizeUnit={"px"}
      size={60}
      color={'#3AD1F1'}
      loading={this.state.loading}
    />;
    return (
      <div user={this.props.user}>
      <HashRouter>
          <React.Suspense fallback={loading()}>
            <Switch>
              {/* <PublicRoutes restricted={true} exact path="/login" name="Login Page" render={props => <Login {...props} />} /> */}
              {/* <Route exact path="/login" name="Register Page" component={Auth(Login, false)} /> */}
              <Route exact path="/login" name="Register Page" component={Login} />
              <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} />
              <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
              <Route exact path="/500" name="Page 500"  render={props => <Page500  {...props}/>} />
              <Route  path="/" name="Home" render={props => <DefaultLayout {...props}/>} />
            </Switch>
          </React.Suspense>
      </HashRouter>
      
      </div>
    );
  }
}

export default App;
